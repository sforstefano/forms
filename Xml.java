package form;
import java.io.File;

import javax.swing.JComboBox;
import javax.swing.JTextArea;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
 
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class Xml {
	
	public static void CrearXml(String nom, String cognom, String correo , String edat ){
		
		try {
			
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			Document doc = docBuilder.newDocument();
			
			//Elemento raiz
			Element rootElement = doc.createElement("persona");
			doc.appendChild(rootElement);
			
			//nombre
			Element nombre = doc.createElement("nombre");
			nombre.appendChild(doc.createTextNode(nom));
			rootElement.appendChild(nombre);
			
			//apellido
			Element apellido = doc.createElement("apellido");
			apellido.appendChild(doc.createTextNode(cognom));
			rootElement.appendChild(apellido);
			
			//correo
			Element mail = doc.createElement("correo");
			mail.appendChild(doc.createTextNode(correo));
			rootElement.appendChild(mail);
			
			//edat
			Element tiempo = doc.createElement("edat");
			tiempo.appendChild(doc.createTextNode(edat));
			rootElement.appendChild(tiempo);
			
			//escribir el contenido en un archivo xml
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult (new File("C:\\Users\\DAW2\\Downloads\\resultado.xml"));
			
			transformer.transform(source, result);
			
	}catch (ParserConfigurationException pce) {
		pce.printStackTrace();
	}catch (TransformerException tfe) {
		tfe.printStackTrace();
	}
	}

}
