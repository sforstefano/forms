package form;
import java.awt.BorderLayout;
import java.awt.Canvas;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Menu;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;



public class form {
	public void Marcop() {
		MarcoPrincipal pmarco= new MarcoPrincipal();
	}
	

}
class MarcoPrincipal extends JFrame{
	MarcoPrincipal(){
		setBounds(550,150,800,600); //posicio del quadro
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); //tancar el quadro
		Content contenido =  new Content();
		add(contenido);
		setSize(800,600);//tamany dels quadre
		setResizable(false);//no pot cambiar el tamany del quadro
		setVisible(true);//fer-lo visible
	}
}

class Content extends JPanel implements ActionListener{
	JTextArea nombre = new JTextArea();
	JTextArea apellido = new JTextArea();
	JTextArea correo = new JTextArea();
	JComboBox<String> edat = new JComboBox<String>(); //lista desplegable de Strings
	JTextField tnombre= new JTextField("Nombre");
	JTextField tapellido= new JTextField("Apellido");
	JTextField tcorreo = new JTextField("Email");
	JTextField tedat = new JTextField("Edat");
	JButton enviar = new JButton("Enviar");
	JButton xml = new JButton("XML");
	Font font = new Font("Crystal", Font.PLAIN,24);
	Font font2 = new Font("Verdana", Font.PLAIN,18);
	
	Content(){
		 setLayout(new GridLayout(9,1));
		 tnombre.setFont(font);//poner un estilo de fuente
		 tnombre.setEditable(false);//no se puede editar
		 tapellido.setFont(font);
		 tapellido.setEditable(false);
		 tcorreo.setFont(font);
		 tcorreo.setEditable(false);
		 tedat.setFont(font);
		 tedat.setEditable(false);
		 nombre.setFont(font2);
		 apellido.setFont(font2);
		 correo.setFont(font2);
		 edat.setFont(font2);
		 
		 edat.addItem("-18"); //poner valores al combo box
		 edat.addItem("18-25");
		 edat.addItem("25-30");
		 edat.addItem("30+");
		 
		 enviar.addActionListener(this);//poner el boton en escucha
		 xml.addActionListener(this);
		 
		 add(tnombre);//a�adir los objetos por orden como quieres que se vean
		 add(nombre);
		 add(tapellido);
		 add(apellido);
		 add(tcorreo);
		 add(correo);
		 add(tedat);
		 add(edat);
		 add(enviar);
		 add(xml);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if(e.getSource()==enviar) {
			//guardar els valors al xml
			String nombre1 = this.nombre.getText();
			System.out.println(nombre1);
			String apellido1= this.apellido.getText();
			System.out.println(apellido1);
			String correo1 = this.correo.getText();
			System.out.println(correo1);
			String edat1= (String) this.edat.getSelectedItem();
			System.out.println(edat1);
			Xml.CrearXml(nombre1, apellido1, correo1, edat1);
		}
		
		if(e.getSource() == xml) {
		//boton para leer el XML
		}
	}
}
